package main

import (
    "./handler"
    "./model"
    "time"
    "github.com/labstack/echo"
    "github.com/labstack/echo/middleware"
    "github.com/labstack/gommon/log"
    mgo "gopkg.in/mgo.v2"
)



// func main() {
    
//     ScheduleWatering(WateringData{time.NewTimer(0), model.Watering{1, false, nil, -1, -1, 5, 0, 0, 0, 0, time.Now(), time.Time{}, 3, 5 * time.Second, false, false, time.Now(), time.Time{}, false}})

//     ScheduleWatering(WateringData{time.NewTimer(0), model.Watering{2, false, nil, -1, -1, -1, 0, 0, 0, 5, time.Now(), time.Time{}, 20, 2 * time.Second, false, false, time.Now(), time.Time{}, false}})

//     for {

//     }
// }



func main() {
    e := echo.New()
    e.Logger.SetLevel(log.ERROR)
    e.Use(middleware.Logger())

    // Database connection
    db, err := mgo.Dial("127.0.0.1")
    if err != nil {
        e.Logger.Fatal(err)
    }

    // Create indices
    if err = db.Copy().DB("sprinkler").C("waterings").EnsureIndex(mgo.Index{
        Key: []string{"_id"},
        Unique: false,
    }); err != nil {
        log.Fatal(err)
    }

    if err = db.Copy().DB("sprinkler").C("zones").EnsureIndex(mgo.Index{
        Key: []string{"_id"},
        Unique: false,
    }); err != nil {
        log.Fatal(err)
    }

    // Initialize handler
    h := &handler.Handler{DB: db}

    var waterings []model.Watering
    db = h.DB.Clone()
    if err = db.DB("sprinkler").C("waterings").
        Find(nil).
        All(&waterings);err != nil {
        return
    }
    defer db.Close()

    for i:= 0; i < len(waterings); i++ {
        handler.ScheduleWatering(h, &handler.WateringData{time.NewTimer(0), false, &waterings[i]})
    }

    // TODO: Read in waterings from database

    // Routes
    e.POST("/waterings", h.CreateWatering)
    e.GET("/waterings/:wateringid", h.FetchWatering)
    e.GET("/waterings/delete/:wateringid", h.DeleteWatering)
    e.GET("/waterings", h.FetchWaterings)
    e.POST("/zones", h.CreateZone)
    e.GET("/zones/:zoneid", h.FetchZone)
    e.GET("/zones/delete/:zoneid", h.DeleteZone)
    e.GET("/zones", h.FetchZones)

    // Start server
    e.Logger.Fatal(e.Start(":6895"))
}