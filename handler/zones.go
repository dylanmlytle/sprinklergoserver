package handler

import (
    "../model"
    "net/http"
    "github.com/labstack/echo"
    "gopkg.in/mgo.v2/bson"
)

func (h *Handler) CreateZone(c echo.Context) (err error) {
    zone := new(model.Zone)
    if err = c.Bind(zone); err != nil {
        return
    }

    var zones []model.Zone
    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("zones").
        Find(nil).
        All(&zones);err != nil {
        return
    }
    defer db.Close()

    var maxZone = 0
    for i := 0; i < len(zones); i++ {
        if zones[i].Number > maxZone {
            maxZone = zones[i].Number
        }
    }

    // Validation
    // if zone.Players[0].Name == "" {
    //     return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Player1 Name"}
    // }

    zone.ID = bson.NewObjectId()
    zone.Number = maxZone + 1

    db = h.DB.Clone()
    defer db.Close()
    if err = db.DB("sprinkler").C("zones").Insert(*zone); err != nil {
        return
    }
    return c.JSON(http.StatusCreated, zone)
}

func (h *Handler) FetchZone(c echo.Context) (err error) {
    zoneid := c.Param("zoneid")

    zone := new(model.Zone)
    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("zones").
        Find(bson.M{"_id": bson.ObjectIdHex(zoneid)}).One(zone);err != nil {
        return
    }
    defer db.Close()

    return c.JSON(http.StatusOK, zone)    
}

func (h *Handler) FetchZones(c echo.Context) (err error) {
    var zones []model.Zone
    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("zones").
        Find(nil).
        All(&zones);err != nil {
        return
    }
    defer db.Close()

    return c.JSON(http.StatusOK, zones)    
}

func (h *Handler) DeleteZone(c echo.Context) (err error) {
    zoneid := c.Param("zoneid")

    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("zones").
        Remove(bson.M{"_id": bson.ObjectIdHex(zoneid)});err != nil {
        return
    }
    defer db.Close()   

    return c.JSON(http.StatusOK, "OK")  
}