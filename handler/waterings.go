package handler

import (
	"../model"
    "net/http"
    "sync"
    "github.com/labstack/echo"
    "gopkg.in/mgo.v2/bson"
    "time"
    "fmt"
    "github.com/stianeikeland/go-rpio"
)

var mutex = &sync.Mutex{}

type WateringData struct {
    Timer *time.Timer
    KillTimerThread bool
    Watering *model.Watering 
}

var wateringDatas []*WateringData

func (h *Handler) CreateWatering(c echo.Context) (err error) {
    watering := new(model.Watering)
    if err = c.Bind(watering); err != nil {
        return
    }

    // Validation
    // if watering.Players[0].Name == "" {
    //     return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Player1 Name"}
    // }

    var zones []model.Zone
    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("zones").
        Find(bson.M{"number": watering.ZoneNumber}).
        All(&zones);err != nil {
        return
    }
    defer db.Close()

    if len(zones) > 1 {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "More than one zone with the same number"}
    }

    if len(zones) == 0 {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "No zones with requested number"}
    }

    watering.ID = bson.NewObjectId()

    ScheduleWatering(h, &WateringData{time.NewTimer(0), false, watering})

    db = h.DB.Clone()
    defer db.Close()
    if err = db.DB("sprinkler").C("waterings").Insert(*watering); err != nil {
        return
    }
    return c.JSON(http.StatusCreated, watering)
}

func (h *Handler) FetchWatering(c echo.Context) (err error) {
    wateringid := c.Param("wateringid")

    watering := new(model.Watering)
    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("waterings").
        Find(bson.M{"_id": bson.ObjectIdHex(wateringid)}).One(watering);err != nil {
        return
    }
    defer db.Close()

    return c.JSON(http.StatusOK, watering)    
}

func (h *Handler) FetchWaterings(c echo.Context) (err error) {
    var waterings []model.Watering
    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("waterings").
        Find(nil).
		All(&waterings);err != nil {
        return
    }
    defer db.Close()

    return c.JSON(http.StatusOK, waterings)    
}

func (h *Handler) DeleteWatering(c echo.Context) (err error) {
    wateringid := c.Param("wateringid")


    for i:= 0; i < len(wateringDatas); i++ {
        fmt.Println(wateringDatas[i].Watering.ID.String(), bson.ObjectIdHex(wateringid).String())
        if wateringDatas[i].Watering.ID.String() == bson.ObjectIdHex(wateringid).String() {
            fmt.Println("Found it")
            wateringDatas[i].KillTimerThread = true
            wateringDatas[i].Timer.Stop()
            wateringDatas = append(wateringDatas[:i], wateringDatas[i+1:]...)
            break
        }
    }

    db := h.DB.Clone()
    if err = db.DB("sprinkler").C("waterings").
        Remove(bson.M{"_id": bson.ObjectIdHex(wateringid)});err != nil {
        return
    }
    defer db.Close() 

    return c.JSON(http.StatusOK, "OK")
}








type WateringTimerTickCallback func(h *Handler, wateringData *WateringData) 

func WateringCallback(h *Handler, wateringData *WateringData) {
    var zone model.Zone
    db := h.DB.Clone()
    var err error
    if err = db.DB("sprinkler").C("zones").
        Find(bson.M{"number": wateringData.Watering.ZoneNumber}).
        One(&zone);err != nil {
        mutex.Unlock()
        return
    }
    defer db.Close()

    if wateringData.Watering.IsCurrentlyOn == false {
        mutex.Lock()
        wateringData.Watering.IsCurrentlyOn = true
        fmt.Println("Sprinkler Zone #", zone.Number, "-", zone.Name, "Turned pin", zone.Pin, "ON @", time.Now())
        wateringData.pin.Low()
    } else {
        wateringData.pin.High()
        wateringData.Watering.IsCurrentlyOn = false
        fmt.Println("Sprinkler Zone #", zone.Number, "-", zone.Name, "Turned pin", zone.Pin, "OFF @", time.Now())
        mutex.Unlock()
    }
}

func ScheduleWatering(h *Handler, wateringData *WateringData) {
    rpio.Open()
    wateringData.pin.Output()
    wateringData.pin.High()
    wateringDatas = append(wateringDatas, wateringData)
    wateringData.Timer = time.NewTimer(GetTimeToNextTick(wateringData.Watering))
    go WateringStartTimer(h, wateringData, WateringCallback)
}

func GetTimeToNextTick(watering *model.Watering) time.Duration {
    now := time.Now()

    if now.Sub(watering.PreviousOnTick) < watering.Duration && watering.IsCurrentlyOn == true {
        return watering.Duration - now.Sub(watering.PreviousOnTick)
    }

    var hourNextTick = watering.RepeatOnHour
    var minuteNextTick = watering.RepeatOnMinute
    var secondNextTick = watering.RepeatOnSecond
    if watering.RepeatOnHour == -1 {
        hourNextTick = watering.PreviousOnTick.Hour()
    }
    if watering.RepeatOnMinute == -1 {
        minuteNextTick = watering.PreviousOnTick.Minute()
    }
    if watering.RepeatOnSecond == -1 {
        secondNextTick = watering.PreviousOnTick.Second()
    }
    nextTick := time.Date(watering.PreviousOnTick.Year(), watering.PreviousOnTick.Month(), watering.PreviousOnTick.Day(), hourNextTick, minuteNextTick, secondNextTick, 0, time.Local)

    if(watering.IsWeeklyRepeat) {
        GetNextWateringTimeForWeekday(watering.Weekdays, &nextTick)
    }

    if nextTick.Before(now) {
        if(watering.IsWeeklyRepeat) {
            nextTick = nextTick.Add(24 * time.Hour)
            GetNextWateringTimeForWeekday(watering.Weekdays, &nextTick)
        } else {
            if(watering.RepeatOnHour == -1 && watering.RepeatOnMinute == -1 && watering.RepeatOnSecond == -1) {
                nextTick = nextTick.Add(time.Duration(watering.DayInterval) * (24 * time.Hour) + time.Duration(watering.HourInterval) * time.Hour + time.Duration(watering.MinuteInterval) * time.Minute + time.Duration(watering.SecondInterval) * time.Second)
            } else {
                if(watering.RepeatOnHour >= 0) {
                    nextTick = nextTick.Add(24 * time.Hour)
                } else if(watering.RepeatOnMinute >= 0) {
                    nextTick = nextTick.Add(time.Hour)
                } else if(watering.RepeatOnSecond >= 0) {
                    nextTick = nextTick.Add(time.Minute)
                } 
            }
        }
    }
    return nextTick.Sub(time.Now())
}

func WateringStartTimer(h *Handler, wateringData *WateringData, wateringTimerTickCallback WateringTimerTickCallback) {
    var occurrences = 0
    for {
        if(time.Now().After(wateringData.Watering.EndTime) && !wateringData.Watering.EndTime.IsZero()) {
            fmt.Println("Reached End Time")
            return
        }
        if(occurrences >= (wateringData.Watering.RepeatOccurrences * 2) && wateringData.Watering.RepeatOccurrences > 0) {
            fmt.Println("Reached Repeat Occurrences")
            return
        }
        <-wateringData.Timer.C
        if wateringData.KillTimerThread == true {
            return
        }
        wateringTimerTickCallback(h, wateringData)
        if(wateringData.Watering.IsCurrentlyOn == true) {
            wateringData.Watering.PreviousOnTick = time.Now();
        }
        wateringData.Timer.Reset(GetTimeToNextTick(wateringData.Watering))
        occurrences++
    }
}

func IsAWateringDayOfWeek(weekdays []time.Weekday, currentWeekday time.Weekday) bool {
    for i := 0; i < len(weekdays); i++ {
        if(currentWeekday == weekdays[i]) {
            return true
        }
    }

    return false
}

func GetNextWateringTimeForWeekday(weekdays []time.Weekday, nextTick *time.Time) {
    for i := 0; i < 7; i++ {
        if(IsAWateringDayOfWeek(weekdays, nextTick.Weekday())) {
            break;
        } else {
            *nextTick = nextTick.Add(24 * time.Hour);
        }
    } 
}