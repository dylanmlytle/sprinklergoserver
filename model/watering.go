package model

import (
    "gopkg.in/mgo.v2/bson"
    "time"
)

type (
    Watering struct {
        ID                    bson.ObjectId    `json:"id" bson:"_id,omitempty"`
        ZoneNumber            int              `json:"zonenumber" bson:"zonenumber"`
        IsWeeklyRepeat        bool             `json:"isweeklyrepeat" bson:"isweeklyrepeat"`
        Weekdays              []time.Weekday   `json:"weekdays" bson:"weekdays"`
        RepeatOnHour          int              `json:"repeatonhour" bson:"repeatonhour"` 
        RepeatOnMinute        int              `json:"repeatonminute" bson:"repeatonminute"` 
        RepeatOnSecond        int              `json:"repeatonsecond" bson:"repeatonsecond"` 
        DayInterval           int              `json:"dayinterval" bson:"dayinterval"`
        HourInterval          int              `json:"hourinterval" bson:"hourinterval"`
        MinuteInterval        int              `json:"minuteinterval" bson:"minuteinterval"`
        SecondInterval        int              `json:"secondinterval" bson:"secondinterval"`
        StartTime             time.Time        `json:"starttime" bson:"starttime"`
        EndTime               time.Time        `json:"endtime" bson:"endtime"`
        RepeatOccurrences     int              `json:"repeatoccurrences" bson:"repeatoccurrences"`
        Duration              time.Duration    `json:"duration" bson:"duration"`
        ManualOverrideOn      bool             `json:"manualoverrideon" bson:"manualoverrideon"`
        ManualOverrideOff     bool             `json:"manualoverrideoff" bson:"manualoverrideoff"`
        PreviousOnTick        time.Time        `json:"previousontick" bson:"previousontick"`
        NextTick              time.Time        `json:"nexttick" bson:"nexttick"`
        IsCurrentlyOn         bool             `json:"iscurrentlyon" bson:"iscurrentlyon"`
    }
)
