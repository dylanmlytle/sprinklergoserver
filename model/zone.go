package model

import (
    "gopkg.in/mgo.v2/bson"
)

type (
    Zone struct {
        ID     bson.ObjectId `json:"id" bson:"_id,omitempty"`
        Number int           `json:"number" bson:"number"`
        Name   string        `json:"name" bson:"name"`
        Pin    int           `json:"pin" bson:"pin"`
    }
)